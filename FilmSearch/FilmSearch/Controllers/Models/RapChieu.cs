﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FilmSearch.Controllers.Models
{
    public class RapChieu
    {
        public RapChieu()
        {
            this.Phong = new List<Phong>();
        }
        public int ID_RapChieu { get; set; }
        public string TenRap { get; set; }
        public string DiaChi { get; set; }
        public string DienThoai { get; set; }
        public int SoPhong { get; set; }
        public virtual ICollection<Phong> Phong { get; set;}
    }
}