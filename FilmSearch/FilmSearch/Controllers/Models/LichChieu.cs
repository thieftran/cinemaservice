﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FilmSearch.Controllers.Models
{
    public class LichChieu
    {
        public LichChieu()
        {
            this.SuatChieu = new List<SuatChieu>();
        }

        public int ID_LichChieu { get; set; }
        public DateTime NgayChieu { get; set; }
        public virtual ICollection<SuatChieu> SuatChieu { get; set; }
    }
}