﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FilmSearch.Controllers.Models
{
    public class SuatChieu
    {
        public SuatChieu()
        {
            this.Ve = new List<Ve>();
        }
        public int ID_SuatChieu { get; set; }
        public Nullable<int> MaLichChieu { get; set; }
        public DateTime GioChieu { get; set; }
        public Nullable<int> MaPhong { get; set; }
        public Nullable<int> MaPhim { get; set; }
        public virtual LichChieu LichChieu { get; set; }
        public virtual Phong Phong { get; set; }
        public virtual Phim Phim { get; set; }
        public virtual ICollection<Ve> Ve { get; set; }
    }
}