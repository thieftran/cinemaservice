﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FilmSearch.Controllers.Models
{
    public class LoaiKhachHang
    {
        public LoaiKhachHang()
        {
            this.KhachHang = new List<KhachHang>();
        }
        public string MaLoaiKH { get; set; }
        public string TenLoaiKH { get; set; }
        public virtual ICollection<KhachHang> KhachHang { get; set; }
    }
}