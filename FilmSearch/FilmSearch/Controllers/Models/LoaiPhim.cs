﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FilmSearch.Controllers.Models
{
    public partial class LoaiPhim
    {
        public LoaiPhim()
        {
            this.Phim = new List<Phim>();
        }

        public string MaLoaiPhim { get; set; }
        public string TenLoaiPhim { get; set; }
        public virtual ICollection<Phim> Phim { get; set; }
    }
}