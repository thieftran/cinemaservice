﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FilmSearch.Controllers.Models
{
    public class Phong
    {
        public Phong()
        {
            this.Ghe = new List<Ghe>();
            this.SuatChieu = new List<SuatChieu>();
        }
        public int ID_Phong { get; set; }
        public string TenPhong { get; set; }
        public int SoLuongGheToiDa { get; set; }
        public string TrangThai { get; set; }
        public Nullable<int> MaRap { get; set; }
        public virtual RapChieu RapChieu { get; set; }
        public virtual ICollection<Ghe> Ghe { get; set; }
        public virtual ICollection<SuatChieu> SuatChieu { get; set; }
    }
}