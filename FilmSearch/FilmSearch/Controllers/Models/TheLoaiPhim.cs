﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FilmSearch.Controllers.Models
{
    public class TheLoaiPhim
    {
        public TheLoaiPhim()
        {
            this.Phim = new List<Phim>();
        }

        public string MaTheLoai { get; set; }
        public string TenTheLoai { get; set; }
        public virtual ICollection<Phim> Phim { get; set; }
    }
}