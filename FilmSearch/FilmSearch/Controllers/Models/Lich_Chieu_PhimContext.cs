﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using FilmSearch.Controllers.Models.Mapping;

namespace FilmSearch.Controllers.Models
{
    public partial class Lich_Chieu_PhimContext : DbContext
    {
        static Lich_Chieu_PhimContext()
        {
            Database.SetInitializer<Lich_Chieu_PhimContext>(null);
        }

        public Lich_Chieu_PhimContext() 
            : base("Name=Lich_Chieu_PhimContext")
        {
        }

        public DbSet<LoaiPhim> LoaiPhim { get; set; }
        public DbSet<TheLoaiPhim> TheLoaiPhim { get; set; }
        public DbSet<Phim> Phim { get; set; }
        public DbSet<RapChieu> RapChieu { get; set; }
        public DbSet<Phong> Phong { get; set; }
        public DbSet<Ghe> Ghe { get; set; }
        public DbSet<LichChieu> LichChieu { get; set; }
        public DbSet<SuatChieu> SuatChieu { get; set; }
        public DbSet<LoaiKhachHang> LoaiKhachHang { get; set; }
        public DbSet<KhachHang> KhachHang { get; set; }
        public DbSet<Ve> Ve { get; set; }

       
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new TheLoaiPhimMap());
            modelBuilder.Configurations.Add(new LoaiPhimMap());
            modelBuilder.Configurations.Add(new PhimMap());
            modelBuilder.Configurations.Add(new RapChieuMap());
            modelBuilder.Configurations.Add(new PhongMap());
            modelBuilder.Configurations.Add(new GheMap());
            modelBuilder.Configurations.Add(new LichChieuMap());
            modelBuilder.Configurations.Add(new SuatChieuMap());
            modelBuilder.Configurations.Add(new LoaiKhachHangMap());
            modelBuilder.Configurations.Add(new KhachHangMap());
            modelBuilder.Configurations.Add(new VeMap());
        }
    }
}