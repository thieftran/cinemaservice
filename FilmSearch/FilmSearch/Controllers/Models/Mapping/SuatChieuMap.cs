﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace FilmSearch.Controllers.Models.Mapping
{
    public class SuatChieuMap : EntityTypeConfiguration<SuatChieu>
    {
        public SuatChieuMap()
        {
            //Primary key
            this.HasKey(t => t.ID_SuatChieu);

            //Properties
            
            //Table and Column Mappings
            this.ToTable("SuatChieu");
            this.Property(t => t.ID_SuatChieu).HasColumnName("ID_SuatChieu");
            this.Property(t => t.MaLichChieu).HasColumnName("MaLichChieu");
            this.Property(t => t.GioChieu).HasColumnName("GioChieu");
            this.Property(t => t.MaPhong).HasColumnName("MaPhong");
            this.Property(t => t.MaPhim).HasColumnName("MaPhim");

            //Relationships
            this.HasOptional(t => t.LichChieu).WithMany(t => t.SuatChieu)
                .HasForeignKey(d => d.MaLichChieu);
            this.HasOptional(t => t.Phim).WithMany(t => t.SuatChieu)
                .HasForeignKey(d => d.MaPhim);
            this.HasOptional(t => t.Phong).WithMany(t => t.SuatChieu)
                .HasForeignKey(d => d.MaPhong);
        }
    }
}