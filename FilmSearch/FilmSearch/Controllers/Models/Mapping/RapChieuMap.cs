﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace FilmSearch.Controllers.Models.Mapping
{
    public class RapChieuMap : EntityTypeConfiguration<RapChieu>
    {
        public RapChieuMap()
        {
            //primary
            this.HasKey(t => t.ID_RapChieu);

            //Properties
            this.Property(t => t.TenRap).HasMaxLength(50);
            this.Property(t => t.DienThoai).HasMaxLength(20);
      

            //Table and Column Mappings
            this.ToTable("RapChieu");
            this.Property(t => t.ID_RapChieu).HasColumnName("ID_RapChieu");
            this.Property(t => t.TenRap).HasColumnName("TenRap");
            this.Property(t => t.DienThoai).HasColumnName("DienThoai");
            this.Property(t => t.SoPhong).HasColumnName("SoPhong");
        }
    }
}