﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
namespace FilmSearch.Controllers.Models.Mapping
{
    public class PhongMap : EntityTypeConfiguration<Phong>
    {
        public PhongMap()
        {
            //Primary key
            this.HasKey(t => t.ID_Phong);

            //Properties
            this.Property(t => t.TenPhong).HasMaxLength(50);
            this.Property(t => t.TrangThai).HasMaxLength(50);

            //Table andColumn Mappings
            this.ToTable("Phong");
            this.Property(t => t.ID_Phong).HasColumnName("ID_Phong");
            this.Property(t => t.TenPhong).HasColumnName("TenPhong");
            this.Property(t => t.SoLuongGheToiDa).HasColumnName("SoLuongGheToiDa");
            this.Property(t => t.TrangThai).HasColumnName("TrangThai");
            this.Property(t => t.MaRap).HasColumnName("MaRap");

            //Relationships
            this.HasOptional(t => t.RapChieu).WithMany(t => t.Phong)
                .HasForeignKey(d => d.MaRap);
        }
    }
}