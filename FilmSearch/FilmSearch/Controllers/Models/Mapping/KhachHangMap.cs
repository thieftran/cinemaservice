﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace FilmSearch.Controllers.Models.Mapping
{
    public class KhachHangMap : EntityTypeConfiguration<KhachHang>
    {
        public KhachHangMap()
        {
            //Primary key
            this.HasKey(t => t.CMND);

            //Properties
            this.Property(t => t.HoTen).HasMaxLength(50);
            this.Property(t => t.DiaChi).HasMaxLength(100);
            this.Property(t => t.DienThoai).HasMaxLength(20);
            this.Property(t => t.Email).HasMaxLength(50);
            this.Property(t => t.SoTaiKhoan).HasMaxLength(20);

            //Table and Column Mappings
            this.ToTable("KhachHang");
            this.Property(t => t.CMND).HasColumnName("CMND");
            this.Property(t => t.HoTen).HasColumnName("HoTen");
            this.Property(t => t.NgaySinh).HasColumnName("NgaySinh");
            this.Property(t => t.GioiTinh).HasColumnName("GioiTinh");
            this.Property(t => t.DiaChi).HasColumnName("DiaChi");
            this.Property(t => t.DienThoai).HasColumnName("DienThoai");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.SoTaiKhoan).HasColumnName("SoTaiKhoan");
            this.Property(t => t.MaLoaiKH).HasColumnName("MaLoaiKH");

            //Relationships
            this.HasOptional(t => t.LoaiKhachHang).WithMany(t => t.KhachHang)
                .HasForeignKey(d => d.MaLoaiKH);
        }
    }
}