﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace FilmSearch.Controllers.Models.Mapping
{
    public class VeMap : EntityTypeConfiguration<Ve>
    {
        public VeMap()
        {
            //Primary key
            this.HasKey(t => t.MaVe);
          
            //Properties
           
            //Table and Column Mappings
            this.ToTable("Ve");
            this.Property(t => t.MaVe).HasColumnName("MaVe");
            this.Property(t => t.GiaVe).HasColumnName("GiaVe");
            this.Property(t => t.ID_SuatChieu).HasColumnName("SuatChieu");
            this.Property(t => t.STTGhe).HasColumnName("Ghe");
            this.Property(t => t.TTDayGhe).HasColumnName("DayGhe");
            this.Property(t => t.ID_KhachHang).HasColumnName("KhachHang");

            //Relationships
            this.HasOptional(t => t.SuatChieu).WithMany(t => t.Ve)
                .HasForeignKey(d => d.ID_SuatChieu);
            this.HasOptional(t => t.Ghe).WithMany(t => t.Ve)
                .HasForeignKey(d => d.STTGhe);
            this.HasOptional(t => t.Ghe).WithMany(t => t.Ve)
                .HasForeignKey(d => d.TTDayGhe);
            this.HasOptional(t => t.KhachHang).WithMany(t => t.Ve)
                .HasForeignKey(d => d.ID_KhachHang);
        }
    }
}