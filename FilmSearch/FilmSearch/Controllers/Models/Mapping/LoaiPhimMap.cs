﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace FilmSearch.Controllers.Models.Mapping
{
    public class LoaiPhimMap : EntityTypeConfiguration<LoaiPhim>
    {
        public LoaiPhimMap()
        {
            //primary key
            this.HasKey(t => t.MaLoaiPhim);

            //properties
            this.Property(t => t.TenLoaiPhim).HasMaxLength(50);

            //Table and Column Mappings
            this.ToTable("LoaiPhim");
            this.Property(t => t.MaLoaiPhim).HasColumnName("MaLoaiPhim");
            this.Property(t => t.MaLoaiPhim).HasColumnName("TenLoaiPhim");
        }
    }
}