﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace FilmSearch.Controllers.Models.Mapping
{
    public class PhimMap : EntityTypeConfiguration<Phim>
    {
        public PhimMap()
        {
            //Primary
            this.HasKey(t => t.ID_Phim);

            //Properties
            this.Property(t => t.TenPhim).HasMaxLength(50);
            this.Property(t => t.GhiChu).HasMaxLength(100);

            //Table adn Column Mappings
            this.ToTable("Phim");
            this.Property(t => t.ID_Phim).HasColumnName("ID_Phim");
            this.Property(t => t.TenPhim).HasColumnName("TenPhim");
            this.Property(t => t.DoDai).HasColumnName("DoDai");
            this.Property(t => t.NgayBatDau).HasColumnName("NgayBatDau");
            this.Property(t => t.NgayKetThuc).HasColumnName("NgayKetThuc");
            this.Property(t => t.MoTa).HasColumnName("MoTa");
            this.Property(t => t.GhiChu).HasColumnName("GhiChu");
            this.Property(t => t.MaLoaiPhim).HasColumnName("LoaiPhim");
            this.Property(t => t.TheLoai).HasColumnName("TheLoai");

            //Relationships
            this.HasOptional(t => t.LoaiPhim).WithMany(t => t.Phim).HasForeignKey(d => d.MaLoaiPhim);
            this.HasOptional(t => t.TheLoaiPhim).WithMany(t => t.Phim).HasForeignKey(d => d.TheLoai);
        }
    }
}