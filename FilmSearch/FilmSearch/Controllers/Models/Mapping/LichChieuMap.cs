﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace FilmSearch.Controllers.Models.Mapping
{
    public class LichChieuMap : EntityTypeConfiguration<LichChieu>
    {
        public LichChieuMap() 
        {
            //primary key
            this.HasKey(t => t.ID_LichChieu);

            //properties
            

            //Table and Column Mappings
            this.ToTable("LichChieu");
            this.Property(t => t.ID_LichChieu).HasColumnName("ID_LichChieu");
            this.Property(t => t.NgayChieu).HasColumnName("NgayChieu");
        }
    }
}