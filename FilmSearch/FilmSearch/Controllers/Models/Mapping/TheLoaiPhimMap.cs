﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace FilmSearch.Controllers.Models.Mapping
{
    public class TheLoaiPhimMap : EntityTypeConfiguration<TheLoaiPhim>
    {
        public TheLoaiPhimMap()
        {
            //primary key
            this.HasKey(t => t.MaTheLoai);

            //properties
            this.Property(t => t.TenTheLoai).HasMaxLength(50);

            //Table and Column Mappings
            this.ToTable("TheLoaiPhim");
            this.Property(t => t.MaTheLoai).HasColumnName("MaTheLoai");
            this.Property(t => t.TenTheLoai).HasColumnName("TenTheLoai");
        }
    }
}