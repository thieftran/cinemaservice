﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace FilmSearch.Controllers.Models.Mapping
{
    public class LoaiKhachHangMap : EntityTypeConfiguration<LoaiKhachHang>
    {
        public LoaiKhachHangMap()
        {
            //primary key
            this.HasKey(t => t.MaLoaiKH);

            //properties
            this.Property(t => t.TenLoaiKH).HasMaxLength(50);

            //Table and Column Mappings
            this.ToTable("LoaiKhachHang");
            this.Property(t => t.MaLoaiKH).HasColumnName("MaLoaiKH");
            this.Property(t => t.TenLoaiKH ).HasColumnName("TenLoaiKH");
        }
    }
}