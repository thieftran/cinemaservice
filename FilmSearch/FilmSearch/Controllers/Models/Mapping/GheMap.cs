﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace FilmSearch.Controllers.Models.Mapping
{
    public class GheMap : EntityTypeConfiguration<Ghe>
    {
        public GheMap()
        {
            //Primary key
            this.HasKey(t => t.STTGhe);
            this.HasKey(t => t.DayGhe);

            //Properties
            this.Property(t => t.LoaiGhe).HasMaxLength(20);
            this.Property(t => t.TrangThai).HasMaxLength(50);

            //Table and Column Mappings
            this.ToTable("Ghe");
            this.Property(t => t.STTGhe).HasColumnName("STTGhe");
            this.Property(t => t.DayGhe).HasColumnName("DayGhe");
            this.Property(t => t.MaPhong).HasColumnName("MaPhong");
            this.Property(t => t.LoaiGhe).HasColumnName("LoaiGhe");
            this.Property(t => t.TrangThai).HasColumnName("TrangThai");

            //Relationships
            this.HasOptional(t => t.Phong).WithMany(t => t.Ghe)
                .HasForeignKey(d => d.MaPhong);
        }
    }
}