﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FilmSearch.Controllers.Models
{
    public class FilmInfo
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}