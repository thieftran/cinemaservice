﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FilmSearch.Controllers.Models
{
    public class Ghe
    {
        public Ghe()
        {
            this.Ve = new List<Ve>();
        }
        public int STTGhe { get; set; }
        public string DayGhe { get; set; }
        public Nullable<int> MaPhong { get; set; }
        public string LoaiGhe { get; set; }
        public string TrangThai { get; set; }
        public virtual Phong Phong { get; set; }
        public virtual ICollection<Ve> Ve { get; set; }
    }
}