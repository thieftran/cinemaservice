﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FilmSearch.Controllers.Models
{
    public class KhachHang
    {
        public KhachHang()
        {
            this.Ve = new List<Ve>();
        }
        public string CMND { get; set; }
        public string HoTen { get; set; }
        public DateTime NgaySinh { get; set; }
        public bool GioiTinh { get; set; }
        public string DiaChi { get; set; }
        public string DienThoai { get; set; }
        public string Email { get; set; }
        public string SoTaiKhoan { get; set; }
        public string MaLoaiKH { get; set; }
        public virtual LoaiKhachHang LoaiKhachHang { get; set; }
        public virtual ICollection<Ve> Ve { get; set; }
    }
}