﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FilmSearch.Controllers.Models
{
    public class Phim
    {
        public Phim()
        {
            this.SuatChieu = new List<SuatChieu>();
        }
        public int ID_Phim { get; set; }
        public string TenPhim { get; set; }
        public int DoDai { get; set; }
        public DateTime NgayBatDau { get; set; }
        public DateTime NgayKetThuc { get; set; }
        public string MoTa { get; set; }
        public string GhiChu { get; set; }
        public string MaLoaiPhim { get; set; }
        public string TheLoai { get; set; }
        public virtual LoaiPhim LoaiPhim { get; set; }
        public virtual TheLoaiPhim TheLoaiPhim { get; set; }
        public virtual ICollection<SuatChieu> SuatChieu { get; set; }
    }
}