﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FilmSearch.Controllers.Models
{
    public class Ve
    {
        public string MaVe { get; set; }
        public float GiaVe { get; set; }
        public Nullable<int> ID_SuatChieu { get; set; }
        public Nullable<int> STTGhe { get; set; }
        public string TTDayGhe { get; set; }
        public string ID_KhachHang { get; set; }
        public virtual SuatChieu SuatChieu { get; set; }
        public virtual Ghe Ghe { get; set; }
        public virtual Ghe DayGhe { get; set; }
        public virtual KhachHang KhachHang { get; set; }
    }
}