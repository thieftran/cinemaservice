﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FilmSearch.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Search()
        {
            return View();
        }

        public ActionResult ListFilm()
        {
            return View();
        }


        public ActionResult BookTicket()
        {
            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }
    }
}
