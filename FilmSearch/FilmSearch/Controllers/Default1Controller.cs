﻿using FilmSearch.QueryFilmInfo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FilmSearch.Controllers
{
    public class Default1Controller : Controller
    {
        //
        // GET: /Default1/

        public ActionResult Index()
        {
            GalaxyCinemaQuery glx = new GalaxyCinemaQuery();
            glx.query();

            return View();
        }

    }
}
