﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FilmSearch.QueryFilmInfo.Models
{
    public class MovieDetails
    {
        public string Name { get; set; }
        public string Detail { get; set; }
        public ShowTime ShowTime { get; set; }

        public MovieDetails()
        {
            ShowTime = new ShowTime();
        }
    }
}