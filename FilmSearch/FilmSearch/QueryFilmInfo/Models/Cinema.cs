﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FilmSearch.QueryFilmInfo.Models
{
    public class CinemaShowTime
    {
        public string Name {get; set;}
        public List<ShowTimeDetail> ShowList = new List<ShowTimeDetail>();
    }
}
