﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FilmSearch.QueryFilmInfo.Models
{
    public class ShowTimeDetail
    {
        public string Date { get; set; }
        public List<string> Time { set; get; }

        public ShowTimeDetail()
        {
            Time = new List<string>();
        }
    }
}
