﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HtmlAgilityPack;
using FilmSearch.QueryFilmInfo.Models;

namespace FilmSearch.QueryFilmInfo
{
    
    public class GalaxyCinemaQuery
    {
        public static string AbsoluteUrl = "https://www.galaxycine.vn";

        private string getListMovie = @"//*[@id='movieSlide']/div/a";

        private string getListMovieDetail = @"//*[@class='fr movie-detail']";

        private string getMovieShowTime = @"//*[@id='showtime']/div[@class='mov_sc']";

        public void query()
        {
            List<string> ListFilmLinks = getFilmList();
            getFilmInformation(ListFilmLinks);

        }
        private HtmlNodeCollection NodeCollection(string url, string xPath)
        {
            HtmlWeb hw = new HtmlWeb();
            HtmlDocument doc = hw.Load(url);
            HtmlNodeCollection items = doc.DocumentNode.SelectNodes(xPath);
            return items;
        }

        private HtmlNode Node(string url, string xPath)
        {
            HtmlWeb hw = new HtmlWeb();
            HtmlDocument doc = hw.Load(url);
            HtmlNode item = doc.DocumentNode.SelectSingleNode(xPath);
            return item;
        }
        public List<string> getFilmList()
        {
            List<string> result = new List<string>();

            HtmlNodeCollection items = NodeCollection(AbsoluteUrl, getListMovie);

            foreach (var item in items)
            {
                string abc = item.GetAttributeValue("href", "#").Trim();
                result.Add(abc);
            }
            return result;
        }

        public List<MovieDetails> getFilmInformation(List<string> ListLinks)
        {
            List<MovieDetails> ret = new List<MovieDetails>();
            foreach (var link in ListLinks)
            {
                HtmlNode MovieDetailNode= Node(AbsoluteUrl + link,getListMovieDetail);
                
                MovieDetails movieDetail = new MovieDetails();
  
                string detail = MovieDetailNode.InnerHtml;
                movieDetail.Detail = detail;
                string name = MovieDetailNode.SelectSingleNode("div[@class='title-article']").InnerText;
                movieDetail.Name = name;

                HtmlNodeCollection ListCinemas = NodeCollection(AbsoluteUrl + link, getMovieShowTime);
                if (ListCinemas != null)
                {
                    foreach (var cinemasNode in ListCinemas)
                    {
                        CinemaShowTime st = getCenermaShowTime(cinemasNode);
                        movieDetail.ShowTime.Cinemas.Add(st);
                    }
                }
                ret.Add(movieDetail);
            }
            return ret;
        }

        private CinemaShowTime getCenermaShowTime(HtmlNode node)
        {
            CinemaShowTime cinemaShowTime = new CinemaShowTime();
            HtmlNode tempNode = node.SelectSingleNode("span[@class='mov_sc_cine']");
            string cinemaName = tempNode.InnerText.Trim();
            cinemaShowTime.Name = cinemaName;
            HtmlNodeCollection ListDate = node.SelectNodes("div[@class='content']/div");
            foreach (var date in ListDate)
            {
                ShowTimeDetail aaa = getShowTime(date);
                cinemaShowTime.ShowList.Add(aaa);
            }
           
            return cinemaShowTime;
        }

        private ShowTimeDetail getShowTime(HtmlNode node)
        {
            ShowTimeDetail showTimeDetail = new ShowTimeDetail();
            string date = node.SelectSingleNode("span[@class='mov_sc_date']").InnerText.Trim();
            string [] t = date.Split(',');
            try
            {
                date = t[1];
            }
            catch (Exception e)
            {

            }
            date = date.Trim();

            HtmlNodeCollection times = node.SelectNodes("div[@class='mov_sc_time_box']/*");
            foreach (var time in times)
            {
                string ti = time.InnerText.Trim();
                showTimeDetail.Time.Add(ti);
            }
            return showTimeDetail;
        }
    }
}